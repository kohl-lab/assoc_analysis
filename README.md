# 2p_analysis

Files for analysing 2-photon imaging data from structures that come from Jimmy's pipeline

# TODO

- Add link for Jimmy's pipeline

# when working on files

- before starting: pull from bitbucket any updates
- remember to commit and push to bitbucket when finished

# how to use it

Create a my_exp.py file with the following structure:
```python
# Setup analysis variables

mouse_id = "CTBD7.1d"
plot = "True"

# Setup path variables

output = "/path/where/to/save/analysis/files/" # need to check what is this
info_file = output + "name_of_exp_info_file.csv"
filepath = "/path/to/pipeline/output/structures/"
rois_file = "/path/to/file/with/consistent/rois/%s/consistent_rois_file_name.csv" % mouse_id
save_path = "/where/to/save/your/figures/%s/" % mouse_id
pynalysis_path = '/path/to/pynalysis/library' # need to check if I use this
```
Add specific paths. Keep this file out of the version control.

Start from file preprocess.py
 

