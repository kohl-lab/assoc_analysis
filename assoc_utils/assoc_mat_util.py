"""
mat_util.py

This script contains utilities to work with MATLAB structures containing
data from the RSCASSOC project (Kohl lab, Dr. Ana Carolina B Barros). Adapted from the MULTIBAR project.

NOTES on MATLAB structure:
    - to check for reference: item.ref
    - to check class: item.attrs['MATLAB_class']
    - to check if MATLAB str: class == b'char'

Authors: Friedemann Zenke, Colleen Gillon

Date: September, 2019

Note: this code uses python 3.7.

"""
import glob
import copy
import gzip
import os
import warnings

import re
import h5py
import numpy as np
import pandas as pd
import pickle
import scipy.io as scio

from util import gen_util


#############################################
def get_behav_mat(matfile, raise_err=False):
    """
    
    Tries to retrieve the path to the behavioural MATLAB struct that 
    corresponds to the imaging MATLAB struct. Returns it if it is found where 
    expected, and otherwise throws a warning, and returns None or throws an 
    error, if raise_err is True.

    Specifically, expects the final directory in the filename path is an 
    "imaging" directory. If the "imaging" directory is found, looks in a 
    parallel "behaviour" directory for the same MATLAB file name.

    Required args:
        - matfile (str): path to MATLAB imaging struct

    Optional args:
        - raise_err (bool): If True, an error is raised if the behavioural 
                            MATLAB structure isn't found where expected. 
                            Otherwise, a warning is thrown instead.
                            default: False

    Returns:
        - behav_mat (str): path to MATLAB behavioural struct, if found in 
                           expected location. Otherwise, if raise_err is False, 
                           returns None.
    """

    full_direc, matfile_only = os.path.split(matfile)
    main_direc, last_direc = os.path.split(full_direc)

    error_msg = None
    if last_direc == "imaging":
        behav_mat = os.path.join(main_direc, "behaviour", matfile_only)
    else:
        behav_mat = None
        error_msg = ("mat file not located in 'imaging' directory. Therefore, "
            "a path to the corresponding behaviour mat file could not be "
            "traced. Behavioural datastructure will not be used.")
    
    if behav_mat is not None and not os.path.exists(behav_mat):
        behav_mat = None
        error_msg = (f"No behaviour mat file for {matfile_only} found at "
            f"{behav_mat}.")

    if error_msg is not None:
        if raise_err:
            raise ValueError(error_msg)
        else:
            warnings.warn(f"{error_msg}. Behavioural datastructure will not "
            "be used.")

    return behav_mat
    
    
#############################################
def load_mat_struct(filename):
    """
    load_mat_struct(filename)

    Loads and returns MATLAB struct such that field names are made explicit as
    Python object structure.

    Required args:
        - filename (str) : the filename of the file to open

    Returns:
        - (mat_struct): File loaded as a MATLAB structure
    """

    return scio.loadmat(filename, struct_as_record=False, squeeze_me=True)


#############################################
def load_mat_file(filename):
    """
    load_mat_file(filename)

    Loads and returns MATLAB using the old mat_struct load function and if this
    fails, uses the HDF5 procedure.

    Required args:
        - filename (str) : the filename of the file to open

    Returns:
        - (mat_struct): File loaded as a MATLAB structure

    Returns:
        - (mat_struct or dict): File loaded as a MATLAB structure if the old
                                file format if scipy loading is supported,
                                otherwise a nested dictionary if the HDF5
                                procedure must be used
    """

    try:
        mat = load_mat_struct(filename)
    except NotImplementedError:
        print('Not an old MAT struct. Trying to read as HDF5 for MATLAB v7.3 '
            'format instead')
        mat = h5py.File(filename, 'r')
    return mat


#############################################
def get_hdf5group_keys(dat):
    """
    get_hdf5group_keys(dat)

    Creates a list of keys in HDF5 group entry.

    Required args:
        - dat (hdf5 structure): HDF5 group entry

    Returns:
        - keys (list): list of keys
    """

    keys = [str(d) for d in dat]
    return keys


#############################################
def print_file_content(mat):
    """
    print_file_content(mat)

    Prints the session, area, plane tree structure for a multibar file loaded
    from HDF5 MATLAB format.

    Required args:
        - mat (mat_struct): File loaded as a MATLAB structure
    """

    dat = mat['imaging']
    print('imaging/')
    for sess in dat:
        print(' {}'.format(sess))
        for area in dat[sess]:
            print(' ->{} {}'.format(area, ', '.join(dat[sess][area])))


#############################################
def save_zipped_pickle(obj, filename, protocol=-1):
    """
    save_zipped_pickle(obj, filename)

    Compresses and pickles an object under the specified name.

    Required args:
        - obj (data struct): data structure to compress and pickle
        - filename (str)   : filename with which to save the object

    Optional args:
        - protocol (int): pickling protocol
                          default: -1
    """

    with gzip.open(filename, 'wb') as f:
        pickle.dump(obj, f, protocol)


#############################################
def load_zipped_pickle(filename):
    """
    load_zipped_pickle(filename)

    Extracts and loads pickled object with the specified file name.

    Required args:
        - filename (str): filename where the object is saved

    Returns:
        - (dict): returns loaded object or an empty dictionary if the loading
                  fails
    """

    try:
        with gzip.open(filename, 'rb') as f:
            loaded_object = pickle.load(f)
            return loaded_object
    except IOError as err:
        print('Warning: {}.\nEmpty dictionary will be returned.'.format(err))
        return dict()


#############################################
def make_date_key(date):
    date = str(date)
    date = 'date_{}_{}_{}'.format(date[0:4], date[4:6], date[6:8])
    return date


#############################################
def make_area_key(area):
    area = 'area{}'.format(area)
    return area


#############################################
def make_plane_key(plane):
    plane = 'plane{}'.format(plane)
    return plane


#############################################
def get_mat_str(char_struct):
    mat_str = u''.join(map(chr, np.squeeze(char_struct)))
    return mat_str


#############################################
def get_date_keys(f):
    keys = f['imaging'].keys()
    return keys


#############################################
def get_area_keys(f, date):
    keys = f['imaging'][date].keys()
    return keys


#############################################
def get_plane_keys(f, date, area):
    plane_keys = [sub for sub in f['imaging'][date][area] if 'plane' in sub]
    return plane_keys


#############################################
def get_sess_beh_keys(f, date, area):
    sess_beh_keys = [sub for sub in f['imaging'][date][area] if 'sess' in sub]
    return sess_beh_keys


#############################################
def get_planes_sess_beh_keys(f, date, area):
    plane_keys    = get_plane_keys(f, date, area)
    sess_beh_keys = get_sess_beh_keys(f, date, area)

    if len(plane_keys) > 0 and len(sess_beh_keys) != 1:
        raise ValueError('No behaviour for the session!')

    return plane_keys, sess_beh_keys


#############################################
def get_fluo_raw(f, date, area, plane):
    fluo_raw = f['imaging'][date][area][plane]['raw_fluoresence'][()]
    return fluo_raw


#############################################
def get_fluo_corr(f, date, area, plane):
    fluo_corr = f['imaging'][date][area][plane]['fluoresence_corrected'][()]
    return fluo_corr


#############################################
def get_neuropil_raw(f, date, area, plane):
    neuropil_raw = f['imaging'][date][area][plane]['raw_neuropil'][()]
    return neuropil_raw


#############################################
def get_fluo_trialbytrial(f, date, area, plane):
    refs = f['imaging'][date][area][plane]['trialByTrialFlu'][()].squeeze()
    fluo_trialbytrial = [f[ref][()] for ref in refs]
    return fluo_trialbytrial


#############################################
def get_nrois(f, date, area, plane):
    fluo_corr = get_fluo_corr(f, date, area, plane)
    nrois = fluo_corr.shape[1]
    return nrois


#############################################
def get_info(f, date, area, plane, strip_txts=None):
    if strip_txts is None:
        strip_txts = ['Dear Friedemann there are ',
                      ' and a variable number of frames']
    elif not isinstance(strip_txts, list):
        strip_txts = [strip_txts]
    info = get_mat_str(f['imaging'][date][area][plane]['info'])
    for strip_txt in strip_txts:
        info = info.replace(strip_txt, '')
    return info


#############################################
def get_ntrials_incl(f, date, area, plane):
    refs = f['imaging'][date][area][plane]['trialByTrialFlu'][()]
    ntrials = refs.shape[0]
    return ntrials


#############################################
def get_task(f, date, area, sess='session_behaviour'):
    task = get_mat_str(f['imaging'][date][area][sess]['task'])
    return task


#############################################
def get_mouseid_from_file(filename):
    mouseid, _ = os.path.splitext(os.path.split(filename)[-1])
    return mouseid


#############################################
def get_mouseid(f, date, area, sess='session_behaviour'):
    mouseid = get_mat_str(f['imaging'][date][area][sess]['ID'])
    return mouseid


#############################################
def check_mouseid(mouseid, f, date, area, sess='session_behaviour'):
    mouseid_rec = get_mouseid(f, date, area, sess)
    if mouseid_rec not in mouseid:
        raise ValueError(('Mouse ID should be {} by {} was '
                          'recorded.').format(mouseid, mouseid_rec))


#############################################
def get_next_mouse_n(df, mouseid):
    if len(df) == 0:
        mouse_n = 1
    else:
        if mouseid in df['mouseid'].tolist():
            raise ValueError('Mouse {} already in dataframe.'.format(mouseid))
        else:
            mouse_n = np.max(df['mouse_n'].to_numpy()) + 1
    return mouse_n


#############################################
def get_date(f, date, area, sess='session_behaviour'):
    date_str = get_mat_str(f['imaging'][date][area][sess]['date'])
    date_str = date_str.replace('-', '')
    date     = date.strip('date').replace('_', '')
    if date_str != date:
        raise ValueError('Dates conflict: {} vs {}.'.format(date, date_str))
    return date_str


#############################################
def get_prop_go(f, date, area, sess='session_behaviour'):
    ref = f['imaging'][date][area][sess]['percent_go'][0][0]
    prop_go = float(get_mat_str(f[ref]))
    return prop_go


#############################################
def get_stim_pos(f, date, area, sess='session_behaviour'):
    stim_pos = f['imaging'][date][area][sess]['stim_position'][()].squeeze().astype(int)
    return stim_pos


#############################################
def get_go_pos(f, date, area, sess='session_behaviour'):
    ref = f['imaging'][date][area][sess]['go_position'][0][0]
    go_pos = int(get_mat_str(f[ref]))
    return go_pos


#############################################
def get_nogo_pos(f, date, area, sess='session_behaviour'):
    ref = f['imaging'][date][area][sess]['go_position'][1][0]
    nogo_pos = int(get_mat_str(f[ref]))
    return nogo_pos


#############################################
def get_trial_outc(f, date, area, sess='session_behaviour'):
    tr_refs = f['imaging'][date][area][sess]['trial_type'][()].ravel()
    tr_out = [get_mat_str(f[ref]) for ref in tr_refs]
    return tr_out


#############################################
def get_run_speed(f, date, area, sess='session_behaviour'):
    run_speed = f['imaging'][date][area][sess]['velocity'][()].squeeze()
    return run_speed


#############################################
def get_water_on(f, date, area, sess='session_behaviour'):
    # subtract 1 as matlab indexes from 1
    water_on = f['imaging'][date][area][sess]['waterON'][()].squeeze().astype(int) - 1
    return water_on


#############################################
def get_licks(f, date, area, sess='session_behaviour'):
    # subtract 1 as matlab indexes from 1
    licks = f['imaging'][date][area][sess]['licks'][()].squeeze().astype(int) - 1
    # occasional occurrence of 9223372036854775807 in this array
    licks = np.delete(licks, np.where(licks == 9223372036854775807))
    return licks


#############################################
def get_motor_arr(f, date, area, sess='session_behaviour'):
    # subtract 1 as matlab indexes from 1
    motor_origin   = f['imaging'][date][area][sess]['motor_atOrigin'][()].squeeze().astype(int) - 1
    motor_back     = f['imaging'][date][area][sess]['motor_back'][()].squeeze().astype(int) - 1
    motor_start    = f['imaging'][date][area][sess]['motor_start'][()].squeeze().astype(int) - 1
    motor_whisk    = f['imaging'][date][area][sess]['motor_atWhisk'][()].squeeze().astype(int) - 1
    motor_lin_back = f['imaging'][date][area][sess]['linear_back'][()].squeeze().astype(int) - 1
    return motor_origin, motor_back, motor_start, motor_whisk, motor_lin_back


#############################################
def get_fps(f, date, area, plane):
    fps = float(f['imaging'][date][area][plane]['fRate'][()].squeeze())
    return fps


#############################################
def get_roipos(f, date, area, plane):
    roipos = f['imaging'][date][area][plane]['position'][()]
    return roipos


#############################################
def get_all_behav_trials(behav_mat, task, date, area, trial_types=None):
    """
    get_all_behav_trials(behav_mat, task, date, area)

    Returns trial types from the behavioural MATLAB structure.

    Required args:
        - behav_mat (str): path to behavioural MATLAB structure.
        - task (str)     : task
        - date (str)     : date formatted as "date_YYYY_MM_DD"
        - area (str)     : area formatted as "areaX"
    
    Optional args:
        - trial_types (list): allowed trial types. If not None, trial types 
                              found in the behavioural data structure are 
                              compared against allowed trial types and an 
                              error is raised if unexpected values are found.
                              default: None

    Returns:
        - all_trials (list): list of all trial types recorded
    """

    behav_str = load_mat_struct(behav_mat)
    all_trials = getattr(
        getattr(
            getattr(behav_str["behavStruct"], task), 
                date), 
                    area
                        ).trial_type.tolist()

    if trial_types is not None:
        unexpected_types = set(all_trials) - set(trial_types)
        if len(unexpected_types):
            unexpected_types = ", ".join(
                [str(trial_type) for trial_type in unexpected_types]
                )
            raise ValueError("Found unexpected trial types "
                f"{unexpected_types} in behavioural data structure.")
    
    return all_trials


#############################################
def add_trial_info(df, row_n, n_trials_incl, f, date, area, 
                   sess="session_behaviour", behav_mat=None):
    """
    add_trial_info(df, row_n, n_trials, f, date, area)

    Returns dataframe with trial information added to the current dataframe row, 
    for a specific date, area and plane from a multibar file loaded from HDF5 
    MATLAB format. 

    Required args:
        - df (pd DataFrame)      : mouse dataframe
        - row_n (int)            : current row
        - n_trials_incl (int)    : number of trials to include
        - f (HDF5 dataset handle): handle for the HDF5 multibar file
        - date (str)             : date formatted as "date_YYYY_MM_DD"
        - area (str)             : area formatted as "areaX"

    Optional args:
        - sess (str)     : session key
                           default: "session_behaviour"
        - behav_mat (str): path to behavioural MATLAB structure, which, if not 
                           None, is used instead of the imaging structure to 
                           calculate the total number of trials and mouse 
                           performance.
                           default: None 

    Returns:
        - df (pd DataFrame): mouse dataframe with row updated
    """

    df = copy.deepcopy(df)
    keys = f["imaging"][date][area][sess].keys()
    if "percent_go" in keys:
        df.loc[row_n, "prop_go"] = get_prop_go(f, date, area, sess)
        df.loc[row_n, "go_pos"]  = get_go_pos(f, date, area, sess)

        # get info from all trials that were fully recorded during imaging
        all_trials_rec = get_trial_outc(f, date, area, sess)[: n_trials_incl]
        df_lab = ["corr_acc", "corr_rej", "false_pos", "missed", "too_soon"]
        h5_lab = ["correct", "correct_rejection", "false_positive", "missed", 
            "too_soon"]
        check = 0
        for lab, hlab in zip(df_lab, h5_lab):
            n = all_trials_rec.count(hlab)
            df.loc[row_n, lab] = n
            check += n
        if check != len(all_trials_rec): 
            raise ValueError(f"{n_trials_incl - check} trial results not "
                "identified in trial list.")
        
        # get info from all trials (from behavioural structure, if available)
        if behav_mat is not None:
            task = get_task(f, date, area, sess)
            all_trials = get_all_behav_trials(
                behav_mat, task, date, area, h5_lab
                )
        else:
            all_trials = get_trial_outc(f, date, area, sess)

        df.loc[row_n, "n_trials"] = len(all_trials)

        corr_acc_all = all_trials.count(h5_lab[0])
        corr_rej_all = all_trials.count(h5_lab[1])

        df.loc[row_n, "prop_corr"] = \
            (corr_acc_all + corr_rej_all) / df.loc[row_n, "n_trials"]
    
    return df


#############################################
def extend_mouse_df(df, filename, behav_mat=None):
    """
    extend_mouse_df(df, filename)

    Returns dataframe with trial information added from a specific multibar 
    file loaded from HDF5 MATLAB format. 

    Required args:
        - df (pd DataFrame): mouse dataframe
        - filename (str)   : filename

    Optional args:
        - behav_mat (str): path to behavioural MATLAB structure, which, if not 
                           None, is used instead of the imaging structure to 
                           calculate the total number of trials and mouse 
                           performance.
                           default: None 

    Returns:
        - df (pd DataFrame): mouse dataframe updated using the filename
    """

    print(filename)
    df = copy.deepcopy(df)
    if df is None:
        df = pd.DataFrame(
            columns=['mouse_n', 'mouseid', 'sess_n', 'date',
                'task', 'area', 'prop_go', 'go_pos'])
    mouseid = get_mouseid_from_file(filename)
    mouse_n  = get_next_mouse_n(df, mouseid)
    with h5py.File(filename, 'r') as f:
        dates = get_date_keys(f)
        sess_n = 0
        for date in dates:
            areas = get_area_keys(f, date)
            sess_n += 1
            for area in areas:
                row_n = len(df)
                df.loc[row_n, 'sess_n'] = sess_n
                df.loc[row_n, 'area'] = area.replace('area', '')
                planes, sess_beh = get_planes_sess_beh_keys(f, date, area)
                n_trials_incl = -1
                for plane in planes:
                    p = plane.replace('plane', '')
                    df.loc[row_n, 'plane_{}'.format(p)] = ''
                    df.loc[row_n, 'depth_{}'.format(p)] = ''
                    df.loc[row_n, 'layer_{}'.format(p)] = ''
                    df.loc[row_n, 'level_{}'.format(p)] = ''
                    df.loc[row_n, 'nrois_{}'.format(p)] = get_nrois(
                        f, date, area, plane)
                    df.loc[row_n, 'info_{}'.format(p)] = get_info(
                        f, date, area, plane)
                    n_trials_incl = get_ntrials_incl(f, date, area, plane)
                df.loc[row_n, 'n_trials_rec'] = n_trials_incl
                for sess in sess_beh:
                    df.loc[row_n, 'task'] = get_task(f, date, area, sess)
                    check_mouseid(mouseid, f, date, area, sess)
                    df.loc[row_n, 'mouse_n'] = mouse_n
                    df.loc[row_n, 'mouseid'] = mouseid
                    df.loc[row_n, 'date'] = get_date(f, date, area, sess)
                    df = add_trial_info(
                        df, row_n, n_trials_incl, f, date, area, sess)
    return df


#############################################
def build_mouse_df(direc, omit=['CGCC8.5a', 'CTBD2.5d']):
    """
    build_mouse_df(direc)

    Creates dataframe with trial information for the files found in 
    the specified directory. 
    
    Saves dataframe under "mouse_df.csv" in the specified directory.

    Required args:
        - direc (str): directory where the mouse files are stored

    Optional args:
        - omit (list): mouse files to omit
                       default: ["CGCC8.5a", "CTBD2.5d"]
    """

    if omit is None:
        omit = []

    mats = np.asarray(glob.glob(os.path.join(direc, '*.mat')))

    # sort IDs (expected with prefix, number and final letter, e.g. CTBD7.2e)
    id_parts = np.empty([len(mats), 3], dtype='S10')
    rem = []
    for m, mat in enumerate(mats):
        mouseid = get_mouseid_from_file(mat)
        if mouseid in omit: # skip
            rem.append(m)
            continue
        i = re.search('\d', mouseid).start()
        id_parts[m, 0] = mouseid[: i]
        id_parts[m, 1] = mouseid[i : -1]
        id_parts[m, 2] = mouseid[-1]

    # remove mats to omit
    mats     = np.delete(mats, rem)
    id_parts = np.delete(id_parts, rem, axis=0)

    sorter, _ = gen_util.hierarch_argsort(
        id_parts, sorter='fwd', axis=1, dtypes=[None, float, None])
    mats      = mats[sorter]

    df = None
    for mat in mats:
        behav_mat = get_behav_mat(mat)     
        df = extend_mouse_df(df, mat, behav_mat)
    df.to_csv(os.path.join(direc, 'mouse_df.csv'))


