"""
session.py

Class to store, extract, and analyze a session recording for
data from the RSCASSOC project (Kohl lab, Dr. Ana Carolina Barros)..

Authors: Colleen Gillon, Ana Carolina Barros

Date: June, 2020

Note: this code uses python 3.7.

"""

import os

import h5py
import numpy as np
import pandas as pd
# import scipy.stats as st
# from sklearn.decomposition import PCA, NMF

from assoc_utils import assoc_mat_util
from util import file_util, gen_util, math_util
# from OASIS_master.oasis.functions import deconvolve


#############################################
#############################################
class Session(object):
    """
    The Session object is the top-level object for analyzing a session
    recording for one area for the Kohl lab RSCASSOC Project. All that needs
    to be provided to create the object is the data directory, mouse ID or
    number, the session date or number and the area number. The Session object
    that is created will contain all of the information relevant to a
    session, including stimulus information, behaviour information and
    pointers to the 2p data.
    """

    def __init__(self, datadir, area, mouseid=None, date=None, mouse_n=None,
                 sess_n=None, mouse_df='exp_info.csv'):
        """
        self.__init__(datadir, area)

        Initializes and returns the new Session object using the specified
        data directory, mouse ID or number, session date or number and
        area number.

        Sets attributes:
            - datapath (str): path to the data file (.mat)
            - home (str)    : path of the master data directory

        and calls self._init_attribs() and
                  self._get_mat_data()

        Required args:
            - datadir (str): full path to the directory where data is stored
            - area (int)   : area

        Optional args:
            - mouseid (str)        : mouse ID (must provide mouse ID or number,
                                     but not both)
                                     default: None
            - date (str or int)    : date, in YYYYMMDD format (must provide date
                                     or session number, but not both)
                                     default: None
            - mouse_n (str or int) : mouse number (must provide mouse ID or
                                     number, but not both)
                                     default: None
            - sess_n (str or int)  : session number (must provide date or
                                     session number, but not both)
                                     default: None
            - mouse_df (str)       : path name of dataframe containing
                                     information on each recorded session.
                                     Dataframe should have the following
                                     columns:
                                         mouse_n, mouseid, sess_n, date, task,
                                         area, prop_go, go_pos, n_trials_rec,
                                         corr_acc, corr_rej, false_pos, missed,
                                         too_soon, n_trials, correct, as well
                                         as columns needed for Plane object
                                     default: 'mouse_df.csv'
            - plane_oi (str or int): plane of interest
                                     default: 'both'
        """

        self.home = datadir

        self._init_attribs(area, mouseid, date, mouse_n, sess_n, mouse_df)

        self.datapath = os.path.join(self.home, '{}.mat'.format(self.mouseid))

        self._get_mat_data()

    #############################################
    def __repr__(self):
        return (f'{self.__class__.__name__} '
                f'(M{self.mouse_n}, S{self.sess_n}, A{self.area})')

    def __str__(self):
        return repr(self)

    #############################################
    def _init_attribs(self, area, mouseid=None, date=None, mouse_n=None,
                      sess_n=None, mouse_df='mouse_df.csv'):
        """
        self._init_attribs(area)

        Sets the attributes for a specific session and area based on the mouse
        dataframe:

        Sets attributes:
            - area (int)        : area
            - date (int)        : session date (in YYYYMMDD format)
            - mouseid (str)     : mouse ID (e.g., CTBD7.2e)
            - mouse_n (int)     : mouse number (corresponds to mouse ID)
            - n_trials_rec (int): number of trials during 2p recording
                                  (-1 if no 2p recording or no ROIs extracted)
            - prop_go (float)   : proportion of GO trials (None if task is
                                  passive)
            - pos_go (int)      : GO bar position (70 if task is passive)
            - sess_n (int)      : session number (corresponds to session date)
            - task (str)        : session task

            number of trials during 2p recording (None if no recording):
            - corr_acc (int)    : correct GO trials
            - corr_rej (int)    : correct NO-GO trials
            - false_pos (int)   : false positive (incorrect GO) trials
            - missed (int)      : missed (incorrect NO-GO) trials
            - prop_corr (float) : proportion of correct trials (too soon trials
                                  ignored)
            - too_soon (int)    : too soon (GO, but too early) trials

        Required args:
            - area (int)   : area

        Optional args:
            - mouseid (str)       : mouse ID (must provide mouse ID or number,
                                    but not both)
                                    default: None
            - date (str or int)   : date, in YYYYMMDD format (must provide date
                                    or session number, but not both)
                                    default: None
            - mouse_n (str or int): mouse number (must provide mouse ID or
                                    number, but not both)
                                    default: None
            - sess_n (str or int) : session number (must provide date or
                                    session number, but not both)
                                    default: None
            - mouse_df (str)      : path name of dataframe containing
                                    information on each recorded session.
                                    Dataframe should have the following
                                    columns:
                                        mouse_n, mouseid, sess_n, date, task,
                                        area, n_trials_rec, n_trials
                                    default: 'mouse_df.csv'
        """

        if isinstance(mouse_df, str):
            mouse_df = file_util.loadfile(mouse_df)

        # collect mouse and session number information
        info = [[mouseid, mouse_n], [date, sess_n]]
        labs = [['mouseid', 'mouse_n'], ['date', 'sess_n']]

        for s in range(len(info)):
            done = False
            if len(info[s]) != 2:
                raise ValueError(
                    'Code must be modified if the sublists are not of '
                    'length 2.')
            for i in range(len(info[s])):
                if info[s][i] is None:
                    if info[s][1 - i] is None:
                        raise ValueError(
                            'Must provide either `{}` or `{}`.'.format(
                                labs[s][i], labs[s][1 - i]))
                    elif done:
                        raise ValueError(
                            'Cannot provide both `{}` and `{}`.'.format(
                                labs[s][i], labs[s][1 - i]))
                    else:
                        if s == 0:
                            info[s][i] = gen_util.get_df_vals(
                                mouse_df, labs[s][1 - i], info[s][1 - i],
                                labs[s][i], single=True)
                        else:
                            info[s][i] = gen_util.get_df_vals(
                                mouse_df, [labs[s][1 - i], 'mouseid'],
                                [info[s][1 - i], info[0][0]],
                                labs[s][i], single=True)
                        done = True

        self.mouseid, self.mouse_n = info[0]
        self.date, self.sess_n = info[1]

        self.area = area
        areas = gen_util.get_df_vals(
            mouse_df, ['mouseid', 'date'], [self.mouseid, self.date], 'area',
            dtype=str)

        if str(self.area) not in areas:
            raise ValueError(
                'No area {} found for mouse {}, {} session.'.format(
                    self.area, self.mouseid, self.date))

        df_line = gen_util.get_df_vals(
            mouse_df, ['mouseid', 'date', 'area'],
            [self.mouseid, self.date, self.area], single=True)

        self.task = df_line['task'].tolist()[0]
        self.exp_n = float(df_line['prop_go'].tolist()[0])
        self.n_trials = int(df_line['n_trials'].tolist()[0])

        keys_int = ['go_pos', 'corr_acc', 'corr_rej', 'false_pos', 'missed',
                    'too_soon']
        vals_int = []
        for key in keys_int:
            val = df_line[key].tolist()[0]
            if np.isnan(val):
                vals_int.append(None)
            else:
                vals_int.append(int(val))

        [self.go_pos, self.corr_acc, self.corr_rej,
         self.false_pos, self.missed, self.too_soon] = vals_int

        # if self.go_pos is None:
        #     self.go_pos = 70
        #
        # if np.isnan(self.prop_go):
        #     self.prop_go = None

        # self.prop_corr = df_line['prop_corr'].tolist()[0]

    #############################################
    def _make_trial_df(self, trial_outc=None, stim_pos=None, n_2pfr=None,
                       go_pos=None, nogo_pos=None):
        """
        self._make_trial_df()

        Saves a dataframe containing trial information as an attribute.

        Sets attributes:
            - stim_pos (list)        : sorted list of stim positions
            - trial_outc (list)      : sorted list of trial outcome types (None
                                       if task is sens_stim)
            - trial_df (pd DataFrame): trial alignment dataframe with columns:
                                       'trial_n', 'stim_pos', 'outcome',
                                       'start2pfr', 'end2pfr', 'num2pfr'
        """

        self.trial_outc = trial_outc
        if self.trial_outc is not None:
            self.trial_outc = sorted(set(self.trial_outc))

        self.trial_df = pd.DataFrame(
            columns=['trial_n', 'stim_pos', 'outcome', 'start2pfr', 'end2pfr',
                     'num2pfr'])

        if trial_outc is None:
            if stim_pos is None:
                raise ValueError('Must pass trial outcome or stim position.')
            else:
                n_trials = len(stim_pos)
                self.trial_df['stim_pos'] = stim_pos
                self.trial_df['outcome'] = [''] * n_trials
        else:
            n_trials = len(trial_outc)
            self.trial_df['outcome'] = trial_outc
            if stim_pos is None:
                stim_pos = [-1] * n_trials
                # add in the go_pos and nogo_pos as stimulus positions
                if go_pos is None or nogo_pos is None:
                    raise ValueError(
                        'If stim_pos is None, go_pos and nogo_pos must be '
                        'provided.')
                trial_outc = np.asarray(trial_outc)
                stim_pos = np.asarray(stim_pos)
                go_trials = np.where(
                    (trial_outc == 'corr_acc') | (trial_outc == 'missed') |
                    (trial_outc == 'too_soon'))
                nogo_trials = np.where(
                    (trial_outc == 'corr_rej') | (trial_outc == 'false_pos'))
                stim_pos[go_trials] = go_pos
                stim_pos[nogo_trials] = nogo_pos
            self.trial_df['stim_pos'] = stim_pos

        self.trial_df['trial_n'] = list(range(n_trials))

        # add the start, end and number of 2p frames
        if len(self.motor_whisk) == len(self.trial_df):
            start2pfr = self.motor_whisk
            end2pfr = (np.asarray(self.motor_whisk)[1:] - 1).tolist()
            num2pfr = \
                (np.asarray(end2pfr) - np.asarray(start2pfr[:-1])).tolist()

            max_num2pfr = np.max(num2pfr).astype(int)
            if n_2pfr is None:
                end2pfr.append(start2pfr + max_num2pfr)
                num2pfr.append(max_num2pfr)
            else:
                end2pfr.append(n_2pfr)
                num2pfr.append(n_2pfr - start2pfr[-1])
                if num2pfr[-1] > max_num2pfr * 1.5:
                    raise ValueError(
                        'Calculation leads to unexpected number '
                        'of frames for the final trial.')
        elif len(self.motor_whisk) > len(self.trial_df):
            n_excess = len(self.motor_whisk) - len(self.trial_df)
            start2pfr = self.motor_whisk[:-n_excess]
            end2pfr = (np.asarray(self.motor_whisk)[1:] - 1).tolist()
            num2pfr = (np.asarray(end2pfr) - np.asarray(start2pfr)).tolist()
            if n_excess > 1:
                print('    WARNING: {} additional trials recorded'.format(
                    n_excess))
        else:
            raise ValueError('All trials were not recorded.')

        self.trial_df['start2pfr'] = start2pfr
        self.trial_df['end2pfr'] = end2pfr
        self.trial_df['num2pfr'] = num2pfr

        self.stim_pos = sorted(self.trial_df['stim_pos'].unique().tolist())

    #############################################
    def _get_mat_data(self):
        """
        self._get_mat_data()

        Sets the attributes for a specific session area based on the mouse data
        file.

        Also calls self._make_df()

        Sets attributes:
            - licks (1D array)         : 2p frames at which each lick started
                                        (None if task is passive)
            - motor_origin (1D array)  : 2p frames at which motor arrived at
                                         origin
            - motor_lin_back (1D array): 2p frames at which motor went back to
                                         origin
            - motor_start (1D array)   : 2p frames at which motor started
                                         toward whisk position
            - motor_whisk (1D array)   : 2p frames at which motor arrived at
                                         whisk position
            - n_trials (int)           : number of trials
            - nogo_pos (int)           : NO-GO bar position  (49 if task is
                                         passive)
            - run (1D array)           : run speed for each 2p frame
            - trial_outc (list)        : outcome of each trial ('correct',
                                         'correct_rejection', 'false_positive',
                                         'missed', 'too_soon')  (None if task
                                         is passive)
            - water_on (1D array)      : 2p frames at which each water onset
                                         occurred (None if task is passive)
        """

        date = assoc_mat_util.make_date_key(self.date)
        area = assoc_mat_util.make_area_key(self.area)

        with h5py.File(self.datapath, 'r') as f:
            [self.motor_origin, _, self.motor_start, self.motor_whisk,
             self.motor_lin_back] = assoc_mat_util.get_motor_arr(
                f, date, area)
            self.run = assoc_mat_util.get_run_speed(f, date, area)

            if self.prop_go is None:
                self.nogo_pos = None
                self.licks = None
                self.water_on = None
                stim_pos = assoc_mat_util.get_stim_pos(f, date, area)
                self.n_trials = np.min([len(self.motor_whisk), len(stim_pos)])
                stim_pos = stim_pos[: self.n_trials]
                self.all_trial_outc = None
            else:
                self.nogo_pos = assoc_mat_util.get_nogo_pos(f, date, area)
                self.licks = assoc_mat_util.get_licks(f, date, area)
                self.water_on = assoc_mat_util.get_water_on(f, date, area)
                self.all_trial_outc = np.asarray(
                    assoc_mat_util.get_trial_outc(f, date, area))
                self.n_trials = np.min(
                    [len(self.motor_whisk), len(self.all_trial_outc)])
                self.all_trial_outc = self.all_trial_outc[: self.n_trials]
                stim_pos = None
                # replace names
                orig = ['correct', 'correct_rejection', 'false_positive']
                new = ['corr_acc', 'corr_rej', 'false_pos']
                for o, n in zip(orig, new):
                    self.all_trial_outc[np.where(self.all_trial_outc == o)] = n

        self._make_trial_df(
            trial_outc=self.all_trial_outc, stim_pos=stim_pos,
            n_2pfr=len(self.run), go_pos=self.go_pos, nogo_pos=self.nogo_pos)

